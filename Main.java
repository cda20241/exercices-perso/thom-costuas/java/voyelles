import java.text.Normalizer;
import java.util.Scanner;

/**
 * This class demonstrates a simple program to count the number of vowels in a given string.
 */
public class Main {

    /**
     * The main entry point for the program.
     *
     * @param args The command-line arguments (not used in this program).
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Entrez votre  phrase : ");
        String inputString = scanner.nextLine();
        int vowelCount = Voyelles(inputString);
        System.out.println("Nombre de voyelles: " + vowelCount);
    }

    /**
     * Counts the number of vowels in a given string.
     *
     * @param inputString The input string to evaluate.
     * @return The count of vowels in the provided string.
     */
    public static int Voyelles(String inputString) {
        char[] vowelsArray = {'a', 'e', 'i', 'o', 'u'};
        int count = 0;
        String lowercaseString = inputString.toLowerCase();
        String normalizedString = removeDiacritics(lowercaseString);

        for (char letter : normalizedString.toCharArray()) {
            for (char vowel : vowelsArray) {
                if (letter == vowel) {
                    count++;
                    break;
                }
            }
        }
        return count;
    }

    /**
     * Removes diacritic marks from a string.
     *
     * @param str The input string with diacritic marks.
     * @return The string with diacritic marks removed.
     */
    private static String removeDiacritics(String str) {
        return Normalizer.normalize(str, Normalizer.Form.NFD)
                .replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
    }
}
